const router = require("express").Router();
const {create, getList, getById, update, deleteItem} = require("../resources/produto.resource");
router.post("/", create);
router.get("/", getList);
router.get("/:id", getById);
router.put("/", update);
router.delete("/:id", deleteItem);
module.exports = router;
