const {
  create, getList, getById, update, deleteItem
} = require("../services/produto.service");

module.exports = {

  create: (req, res) => {
    const body = req.body;
    create(body, (err, response) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "Database connection error"
        });
      } else {
        body.id = response.insertId;
      }
      return res.status(200).json(body);
    });
  },

  getList: (req, res) => {
    getList((err, response) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(response);
    });
  },

  getById: (req, res) => {
    const id = req.params.id;
    getById(id, (err, response) => {
      if (err) {
        console.log(err);
        return;
      }
      if (!response) {
        return res.json({
          success: 0,
          message: "Record not Found"
        });
      }
      return res.json(response);
    });
  },

  update: (req, res) => {
    const body = req.body;
    update(body, (err, response) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: err.sqlMessage
        });
      } else if (response) {
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      }
    });
  },

  deleteItem: (req, res) => {
    const id = req.params.id;
    deleteItem(id, (err, response) => {
      if (err) {
        console.log(err);
        return res.json({
          success: 0,
          message: err.sqlMessage
        });
      } else if(response) {
        if (response.affectedRows == 0) {
          return res.json({
            success: 0,
            message: "Record Not Found"
          });
        }
        return res.json({
          success: 1,
          message: "deleted successfully"
        });
      }
    });
  }



};
