const pool = require("../../config/database");

module.exports = {

  create: (data, callBack) => {
    pool.query(
      `insert into produto(nome, cor, tamanho, valor) values(?,?,?,?)`,
      [
        data.nome,
        data.cor,
        data.tamanho,
        data.valor,
      ],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  getList: callBack => {
    pool.query(
      `select * from produto`,
      [],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  getById: (id, callBack) => {
    pool.query(
      `select * from produto where id = ?`,
      [id],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response[0]);
      }
    );
  },

  update: (data, callBack) => {
    pool.query(
      `update produto set nome=?, cor=?, tamanho=?, valor=? where id = ?`,
      [
        data.nome,
        data.cor,
        data.tamanho,
        data.valor,
        data.id,
      ],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  deleteItem: (id, callBack) => {
    pool.query(
      `delete from produto where id = ?`,
      [id],
      (error, response) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  }


};
