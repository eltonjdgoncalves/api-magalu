require("dotenv").config();
const express = require("express");
const app = express();

const clienteRouter = require("./api/routers/cliente.router");
const produtoRouter = require("./api/routers/produto.router");
const pedidoRouter = require("./api/routers/pedido.router");

app.use(express.json());
app.use("/api/clientes", clienteRouter);
app.use("/api/produtos", produtoRouter);
app.use("/api/pedidos", pedidoRouter);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log("server up and running on PORT :", port);
});
